<?php include MODULE."header.php"; ?>
<body>
    <script src="<?php print(URL); ?>public/js/homeUX.js"></script>
    
	<?php if( isset($this->debug) ) : ?>
		<div id="debug">Offset Top: 0 px</div>
	<?php endif; ?>

	<header>
		<div id="logo"></div>
		<nav>
			
		</nav>
		<div id="login" class="fwRight">
		<?php 
			if(Session::get("username")){
				echo "Hola, ".Session::get("username");
			}else{
				echo "Login";
			}
		?>
		</div>
	</header>
	
	<?php
		if(Session::get("username")){
			$this->usrCtrlr->floatingOptions();
		}else{
			$this->usrCtrlr->floatingForm();
		}
		
	?>

	<div id="mainWrapper">

	    <div class="parallax-window" data-image-src="<?php print(URL); ?>public/assets/images/3.jpg" data-parallax="scroll" style="height:800px !important;"></div>
		
		<div class="separador_titulo">
			Welcome to <?php print($this->title); ?>
		</div>

		<div class="separador">
			<div class="contenido">
				<div class="title">Featured Products</div>
				<div class="fProduct">
					<img src="<?php print(URL); ?>public/assets/images/fProduct.jpg" alt="Producto Recomendado">
				</div>
				<div class="fProduct">
					<img src="<?php print(URL); ?>public/assets/images/fProduct.jpg" alt="Producto Recomendado">
				</div>
				<div class="fProduct">
					<img src="<?php print(URL); ?>public/assets/images/fProduct.jpg" alt="Producto Recomendado">
				</div>
			</div>
		</div>

	    <div class="parallax-window" data-image-src="<?php print(URL); ?>public/assets/images/1.jpg" data-parallax="scroll"></div>
	
		<div class="separador">
			<div id="manita">
				<img src="<?php print(URL); ?>public/assets/images/icon1.png" alt="">
			</div>
			<div class="contenido">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis esse modi blanditiis optio, voluptatibus nobis quas, assumenda ut, nemo error earum placeat nulla! Reiciendis sapiente sequi earum doloremque commodi, ea!

				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, ipsum assumenda ea dolore quia. Corporis, alias, iste. Iure minima dicta quidem modi numquam aut voluptatibus atque. Provident inventore similique nihil!
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, ipsum assumenda ea dolore quia. Corporis, alias, iste. Iure minima dicta quidem modi numquam aut voluptatibus atque. Provident inventore similique nihil!
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, ipsum assumenda ea dolore quia. Corporis, alias, iste. Iure minima dicta quidem modi numquam aut voluptatibus atque. Provident inventore similique nihil!
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, ipsum assumenda ea dolore quia. Corporis, alias, iste. Iure minima dicta quidem modi numquam aut voluptatibus atque. Provident inventore similique nihil!
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, ipsum assumenda ea dolore quia. Corporis, alias, iste. Iure minima dicta quidem modi numquam aut voluptatibus atque. Provident inventore similique nihil!
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, ipsum assumenda ea dolore quia. Corporis, alias, iste. Iure minima dicta quidem modi numquam aut voluptatibus atque. Provident inventore similique nihil!
			</div>
		</div>

	    <div class="parallax-window" data-image-src="<?php print(URL); ?>public/assets/images/2.jpg" data-parallax="scroll"></div>

	    <div class="separador">
			<div class="contenido">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis esse modi blanditiis optio, voluptatibus nobis quas, assumenda ut, nemo error earum placeat nulla! Reiciendis sapiente sequi earum doloremque commodi, ea!

				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, ipsum assumenda ea dolore quia. Corporis, alias, iste. Iure minima dicta quidem modi numquam aut voluptatibus atque. Provident inventore similique nihil!
			</div>
		</div>
    </div>

</body>
</hmtl>


